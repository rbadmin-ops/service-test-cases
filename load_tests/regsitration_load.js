var registration = require('../modules/module.registration.js');
var async = require('async');

var maxConcurrentRequests = 5;
var totalRequestsPerThread = 2000;


function registerUser(threadNumber, registrationNumber) {
	var email = 'cptkirc' + threadNumber + '-' + registrationNumber + '@gmail.com';
    var r = registration.create_registration(email, 'myPassword');
    console.time(email)

    r.registerUser(function(err, response) {
        if (err)
            console.err(err);
        else {
        	console.timeEnd(email);

        	if (registrationNumber < totalRequestsPerThread)
        		registerUser(threadNumber, ++registrationNumber);
        }
    });
}

for (var i = 1; i <= maxConcurrentRequests; i++) {
	registerUser(i, 1);
};