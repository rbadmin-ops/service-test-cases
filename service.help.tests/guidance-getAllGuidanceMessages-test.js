var http = require('http');
var assert = require("assert");
var config = require('../config/config');

describe('Guidance', function() {
    describe('#getAllGuidanceMessages()', function() {
        it('should retrieve all guidance messages', function(done) {
            // Setup the request.  The options parameter is
            // the o
            var headers = {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Auth-Token': '+l+uQlgOPE/WTLPNVt4/HnHmWezjN/4WwHWTBu1dlOe9QioVUWZlKLIZ9Qn3NSdo'
            };

            var options = {
                host: config.host,
                port: 80,
                path: '/guidance.json',
                method: 'GET',
                headers: headers
            };
            var req = http.request(options, function(res) {
                res.setEncoding('utf-8');

                var responseString = '';

                res.on('data', function(data) {
                    responseString += data;
                });

                res.on('end', function() {
                    console.log(responseString);
                    var resultObject = JSON.parse(responseString);
                    assert.equal("success", resultObject.data.message);
                    done();
                });
            });

            req.on('error', function(e) {
                done(e);
            });

            req.end();
        })
    })
})
