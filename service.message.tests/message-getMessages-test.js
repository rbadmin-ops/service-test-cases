var http = require('http');
var assert = require("assert");
var config = require('../config/config');
var message = require('../../rb-service-tools')().Message;
var _ = require('lodash');
//var device = require('rb-services-tools').Device;

describe('Messages', function() {
    describe('#getMessages', function() {
        it('should get user\'s messages ', function(done) {
            message.createV2MessagesServiceInterface(config)
                .getMessages()
                .done(function(response, reject) {
                    if (reject) {
                        done(reject)
                    }

                    console.log(response);

                    done();
                })
        })
    })
})

function sleep (time) {
  return new Promise((resolve) => setTimeout(resolve, time));
}
