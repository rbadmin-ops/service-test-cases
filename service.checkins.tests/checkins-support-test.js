var http = require('http');
var assert = require("assert");
var config = require('../config/config');
var registration = require('../modules/module.registration.js');

var randomEmail = '';

describe('hooks', function() {
    before(function() {

        randomEmail = (makeEmail(15)) + '@gmail.com';

        console.log(randomEmail);

        function makeEmail(len) {
            var text = "";
            var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

            for (var i = 0; i < len; i++)
                text += possible.charAt(Math.floor(Math.random() * possible.length));

            return text;
        }
    })
    after(function() {

    })
    beforeEach(function() {

    })
    afterEach(function() {

    })
})

describe('Checkins', function() {
    var cookies;

    describe('#checkinDateSpan()', function() {
        it('should validate the date range of a relationship restart', function(done) {
            var headers = {
                'Auth-Token': '+l+uQlgOPE/WTLPNVt4/HnHmWezjN/4WwHWTBu1dlOe9QioVUWZlKLIZ9Qn3NSdo'
            };

            var options = {
                host: config.service_host,
                port: config.service_port,
                path: '/checkins/checkinDateSpan/357/13.json',
                method: 'GET',
                headers: headers
            };
            var req = http.request(options, function(res) {
                res.setEncoding('utf-8');

                var responseString = '';

                res.on('data', function(data) {
                    responseString += data;
                });

                res.on('end', function() {
                    cookies = res.headers['set-cookie'];
                    console.log(resultObject);
                    var resultObject = JSON.parse(responseString);
                    console.log(resultObject);
//                    assert.equal("1", resultObject.data.error);
//                    assert.equal("The email address of [" + config.email + "] has already been used.", resultObject.data.message);
                    done();
                });
            });

            req.on('error', function(e) {

                done(e);
            });

            req.write('');
            req.end();
        })
    })
})
