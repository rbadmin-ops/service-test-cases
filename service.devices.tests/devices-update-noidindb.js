var http = require('http');
var assert = require("assert");
var config = require('../config/config');
var device = require('../../rb-service-tools/modules/module.devices')(config);
var _ = require('lodash');

//var device = require('rb-services-tools').Device;

describe('Devices', function() {
    var nextDeviceId;

    describe('#addNewDevice', function() {
        it('should find the last device id used', function(done) {
            device
                .v2.findDevices({}, [
                    {field: 'id', direction: 'desc'}
                ])
                .done(function(response, reject) {
                    if (reject) {
                        done(reject)
                    }

                    assert.equal(Array, response.data.constructor , "Expected response.data to be an array")
                    assert.equal(false, isNaN(response.data[0].id), "Expected id to be an integer.  Received: " + response.data[0].id)

                    nextDeviceId = Number(response.data[0].id) + 1;

                    done();
                })
        }),
        it('should create a new record with the supplied ID', function(done) {
            var deviceObj = device
                .v2.createDeviceModel(2, device.v2.createUniqueGUID(), 'device model', 'device os', 'app version')

            deviceObj.id = nextDeviceId;

            device
                .v2.updateDevice(deviceObj)
                .done(function(response, reject) {
                    if (reject) {
                        done(reject);
                    }

                    done();
                })
        })
    })
})

function sleep (time) {
  return new Promise((resolve) => setTimeout(resolve, time));
}
