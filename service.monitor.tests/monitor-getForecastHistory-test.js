var http = require('http');
var assert = require("assert");
var config = require('../config/config');

describe('Monitor', function() {
    var email = 'dahannajr@gmail.com';

    describe('#getForecastHistory()', function() {
        it('should retrieve checkin summary information for the last month', function(done) {
            // Setup the request.  The options parameter is
            // the o
            var headers = {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Auth-Token': 'fNtG0+cC70JnDoi5komgu3AoI0mBdULxviKINHSS/fFuQ8RVRrOYrcIgZ1SpBxAa'
            };

            var options = {
                host: config.host,
                port: 80,
                path: '/monitor/forecastHistory/4.json',
                method: 'GET',
                headers: headers
            };
            var req = http.request(options, function(res) {
                res.setEncoding('utf-8');

                var responseString = '';

                res.on('data', function(data) {
                    responseString += data;
                });

                res.on('end', function() {
                    console.log(responseString);
                    var resultObject = JSON.parse(responseString);
                    assert.equal("success", resultObject.data.message);
                    done();
                });
            });

            req.on('error', function(e) {
                done(e);
            });

            req.end();
        })
    })
})
