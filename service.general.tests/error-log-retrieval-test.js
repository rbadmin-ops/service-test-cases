var http = require('http');
var assert = require("assert");
var config = require('../config/config');

describe('General Services', function() {
    describe('#error log retrieval()', function() {
        it('should retrieve error logs', function(done) {
            var headers = {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Content-Length': 0
            };

            var options = {
                host: config.service_host,
                port: config.service_port,
                path: '/?errorlog=1&key=q0:brb',
                method: 'GET',
                headers: headers
            };
            var req = http.request(options, function(res) {
                res.setEncoding('utf-8');

                var responseString = '';

                res.on('data', function(data) {
                    responseString += data;
                });

                res.on('end', function() {
                	console.log(responseString);
//                    assert.equal("1", resultObject.data.error);
//                    assert.equal("The email address of [dahannajr@gmail.com] has already been used.", resultObject.data.message);
                    done();
                });
            });

            req.on('error', function(e) {

                done(e);
            });

            req.write('');
            req.end();
        })
    })
})
