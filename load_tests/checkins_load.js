var checkin = require('../modules/module.checkins.js');
var async = require('async');

var maxConcurrentRequests = 20;
var totalRequestsPerThread = 2000;


function randomDate(start, end) {
    return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
}

function makeComment(len) {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < len; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    console.log(text);

    return text;
}

function submitCheckin(threadNumber, checkinNumber) {
    var salt = '+l+uQlgOPE/WTLPNVt4/HnHmWezjN/4WwHWTBu1dlOe9QioVUWZlKLIZ9Qn3NSdo';
    var userneedid = "2094";
    var relationshipid = "4";
    var comment = makeComment(30);
    var checkin_date = randomDate(new Date(2015, 0, 1), new Date());
    var wheel_setting = (Math.ceil(Math.random() * 7 * threadNumber * checkinNumber) % 7) + 1;

    var r = checkin.create_checkin(salt, userneedid, relationshipid, wheel_setting, comment, checkin_date.getTime() / 1000);
    console.time(comment);

    r.submitCheckin(function(err, response) {
        if (err)
            console.err(err);
        else {
            console.timeEnd(comment);

            if (checkinNumber < totalRequestsPerThread)
                submitCheckin(threadNumber, ++checkinNumber);
        }
    });

}

for (var i = 1; i <= maxConcurrentRequests; i++) {
	submitCheckin(i, 1);
};