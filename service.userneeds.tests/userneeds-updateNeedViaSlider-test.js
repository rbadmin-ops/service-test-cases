var http = require('http');
var assert = require("assert");
var config = require('../config/config');


describe('UsersNeeds', function() {
    describe('#updateUsersNeedsSlider()', function() {
        it('should set a new slider value for a specific user need', function(done) {
            // Setup the request.  The options parameter is
            // the object we defined above.

            var need = {
               id: 3,
               name: 'communicates well',
               trinity_id: 2
            }; 

            var needSlider = {
                id: 202,
                slider_value: 50,
                relationship_id: 2
            };

            var usersNeed = {
                id: 202,
                need_id: 3
            };

            var data = {
                Need: need,
                Needslider: needSlider,
                UsersNeed: usersNeed
            }

            var needsString = JSON.stringify({data: data});

            var headers = {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Content-Length': needsString.length,
                'Auth-Token': config.auth_token
            };

            var options = {
                host: config.service_host,
                port: config.service_port,
                path: '/users_needs/' + usersNeed.id + '.json',
                method: 'PUT',
                headers: headers
            };

            var req = http.request(options, function(res) {
                res.setEncoding('utf-8');

                var responseString = '';

                res.on('data', function(data) {
                    responseString += data;
                });

                res.on('end', function() {
                    var resultObject = JSON.parse(responseString);
                    assert.equal("success", resultObject.data.message);
                    done();
                });
            });

            req.on('error', function(e) {
                done(e);
            });

            req.write(needsString);
            req.end();
        })
    })
})
