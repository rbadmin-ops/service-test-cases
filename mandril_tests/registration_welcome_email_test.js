var mandrill = require('mandrill-api/mandrill');
var mandrill_client = new mandrill.Mandrill('foOvaoMZ9MEYpRBRaM6KNg');

// Set to whatever value is stored in the 'Name' field of the Mandrill Template Info page
var template_name = "Registration Welcome";

var template_content = [{"global_merge_vars": []}];;
var message = {
    "to": [{
            "email": "david.hanna@kickstepinc.com",
            "name": "David Hanna",
            "type": "to"
        }],
    "headers": {
        "Reply-To": "support@relationshipbarometer.com"
    },
    global_merge_vars: [{
            "name": "first_name",
            "content": "David"
        }],
    "important": false,
    "track_opens": null,
    "track_clicks": null,
    "auto_text": null,
    "auto_html": null,
    "inline_css": null,
    "url_strip_qs": null,
    "preserve_recipients": null,
    "view_content_link": null,
    "bcc_address": null,
    "tracking_domain": null,
    "signing_domain": null,
    "return_path_domain": null,
    "merge": true,
    "merge_language": "handlebars"
};
var async = false;
var ip_pool = "Main Pool";
var send_at = null;
mandrill_client.messages.sendTemplate({"template_name": template_name, "template_content": template_content, 
	"message": message, "async": async, "ip_pool": ip_pool, "send_at": send_at}, function(result) {
    console.log(result);
}, function(e) {
    // Mandrill returns the error as an object with name and message keys
    console.log('A mandrill error occurred: ' + e.name + ' - ' + e.message);
    // A mandrill error occurred: Unknown_Subaccount - No subaccount exists with the id 'customer-123'
});