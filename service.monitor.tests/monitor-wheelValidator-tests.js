var assert = require("assert");

describe('Monitor', function() {
	var _wheelDegrees = [
		{segment: 0, low_range: -1, high_range: -1},
		{segment: 1, low_range: 330, high_range: 283},
		{segment: 2, low_range: 280, high_range: 232},
		{segment: 3, low_range: 230, high_range: 181},
		{segment: 4, low_range: 178, high_range: 129},
		{segment: 5, low_range: 126, high_range: 78},
		{segment: 6, low_range: 74, high_range: 25},
		{segment: 7, low_range: 382, high_range: 333}];

	var _segmentRanges = [
		{segment: 0, low_range: -1, high_range: -1},
		{segment: 1, low_range: 0, high_range: .143},
		{segment: 2, low_range: .143, high_range: .286},
		{segment: 3, low_range: .286, high_range: .429},
		{segment: 4, low_range: .429, high_range: .571},
		{segment: 5, low_range: .571, high_range: .714},
		{segment: 6, low_range: .714, high_range: .857},
		{segment: 7, low_range: .857, high_range: 1.00}];

    describe('#wheelAngelValidation()', function() {
        it('should update email error', function(done) {

        	var segment = 4;
        	var total_percentage = .429;

        	var segment_span = _segmentRanges[segment].high_range - _segmentRanges[segment].low_range;
        	var total_percentage_offset_within_segment = 
        		(total_percentage - _segmentRanges[segment].low_range) / segment_span;

        	var wheel_span = _wheelDegrees[segment].low_range - _wheelDegrees[segment].high_range;
        	var wheel_position = _wheelDegrees[segment].low_range - (wheel_span * total_percentage_offset_within_segment);

        	console.log(wheel_position);

        	done();
        });
    });
});