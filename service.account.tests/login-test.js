var http = require('http');
var assert = require("assert");
var config = require('../config/config');

describe('User', function() {
    var email = 'dahannajr@'

    describe('#login()', function() {
        it('should login without error', function(done) {
            // Setup the request.  The options parameter is
            // the object we defined above.

            var login = {
                email: config.email,
                password: config.password,
                forgotemail: '',
                datetime: 1432136097
            }; 

            var data = {
                _ajaxrequest: 1,
                User: login
            };

            var user = {
                data: data
            };


            var userString = JSON.stringify(user);
            //var userString = 'data%5B_ajaxrequest%5D=1&data%5BUser%5D%5Bemail%5D=dahannajr%40gmail.com&data%5BUser%5D%5Bpassword%5D=Marvin01&data%5BUser%5D%5Bforgotemail%5D=&data%5BUser%5D%5Bdatetime%5D=1432136097';

            var headers = {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Content-Length': userString.length,
                'Auth-Token': '8fNkeLIEBQwRI7rELi7a+vVQC0b3ITqNgTx9qpIPlIMC+Py1/ALBDiZ80lSqrU6g'
            };

            var options = {
                host: config.service_host,
                port: config.service_port,
                path: '/userprofiles/login.json',
                method: 'POST',
                headers: headers
            };
            var req = http.request(options, function(res) {
                res.setEncoding('utf-8');

                var responseString = '';

                res.on('data', function(data) {
                    responseString += data;
                });

                res.on('end', function() {
                    var resultObject = JSON.parse(responseString);
                    console.log(responseString);
                    assert.equal("success", resultObject.data.message);
                    done();
                });
            });

            req.on('error', function(e) {
                done(e);
            });

            req.write(userString);
            req.end();
        }),

        it('should fail authentication', function(done) {
            // Setup the request.  The options parameter is
            // the object we defined above.

            var login = {
                email: config.email,
                password: config.password + 'sdf',
                forgotemail: '',
                datetime: 1432136097
            }; 

            var data = {
                _ajaxrequest: 1,
                User: login
            };

            var user = {
                data: data
            };


            var userString = JSON.stringify(user);

            var headers = {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Content-Length': userString.length,
                'Auth-Token': '8fNkeLIEBQwRI7rELi7a+vVQC0b3ITqNgTx9qpIPlIMC+Py1/ALBDiZ80lSqrU6g'
            };

            var options = {
                host: config.service_host,
                port: config.service_port,
                path: '/userprofiles/login.json',
                method: 'POST',
                headers: headers
            };

            var req = http.request(options, function(res) {
                res.setEncoding('utf-8');

                var responseString = '';

                res.on('data', function(data) {
                    responseString += data;
                });

                res.on('end', function() {
                    var resultObject = JSON.parse(responseString);
                    assert.equal("1", resultObject.data.error);
                    assert.equal("sign_1", resultObject.data.message);
                    done();
                });
            });

            req.on('error', function(e) {
                done(e);
            });

            req.write(userString);
            req.end();
        })
    })
})
