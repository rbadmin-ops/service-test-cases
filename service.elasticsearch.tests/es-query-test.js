var elasticsearch = require('elasticsearch');
var client = new elasticsearch.Client({
  host: '127.0.0.1:9200',
  log: 'trace'
});

client.ping({
  // ping usually has a 3000ms timeout 
  requestTimeout: Infinity,
 
  // undocumented params are appended to the query string 
  hello: "elasticsearch!"
}, function (error) {
  if (error) {
    console.trace('elasticsearch cluster is down!');
    return;
  } else {
    console.log('All is well');
    return;
  }
});