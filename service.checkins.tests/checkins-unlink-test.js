var http = require('http');
var assert = require("assert");
var config = require('../config/config');
var checkin = require('../modules/module.checkins.js');
var needslidersHistory = require('../modules/module.needsliders-histories').NeedslidersHistory;
var needslider = require('../modules/module.needsliders').Needslider;

var randomComment = '';

describe('Checkins', function() {
    // General variables to be used for sending POST, GET, PUT and DELETE requests
    var needSliderId = 202;
    var salt = config.auth_token;
    var userneedid = "202";
    var relationshipid = "2";
    var wheel_setting = 1;
    var checkin_date = new Date();

    // Variables which will hold interim state information for processing subsequent
    // (it) tests
    var checkinId;
    var needslidersHistoriesCount;
    var needslidersHistoriesMostRecentId;

    var startingSliderValueAsPercentage;

    describe('#unlinkACheckin()', function() {
        before(function() {

            randomComment = (makeComment(30));

            function makeComment(len) {
                var text = "";
                var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

                for (var i = 0; i < len; i++)
                    text += possible.charAt(Math.floor(Math.random() * possible.length));

                return text;
            }
        })
        after(function() {

        })
        beforeEach(function() {

        })
        afterEach(function() {

        })
        it('should get the most recent need slider setting for this relationship/need', function(done) {
            needslider
                .v2.getNeedslider(needSliderId)
                .done(function(response, reject) {
                    if (reject) {
                        done(reject);
                    }

                    assert.strictEqual(response.errors.constructor, Array);
                    assert.strictEqual(response.data.constructor, Object, 'Needslider is not an Object on data.  It is ' + response.data.constructor);
                    startingSliderValueAsPercentage = response.data.attributes.sliderValueAsPercentage;
                    assert.equal(false, isNaN(startingSliderValueAsPercentage), 'Starting need slider value should be a number');
                    assert.equal(true, startingSliderValueAsPercentage > 0, 'Starting need slider value should be greater than 0');
                    assert.equal(true, startingSliderValueAsPercentage < 500, 'Starting need slider value should be less than 500');
                    done();
                })
        }),
        it('should get the most recent need slider history entry for this relationship/need', function(done) {
            needslider
                .v2.getNeedsliderWithHistory(needSliderId)
                .done(function(response, reject) {
                    if (reject)
                        done(reject);

                    assert.equal(response.error, false, response.data);
                    assert.strictEqual(response.data.needsliderHistories.constructor, Array, response.data.constructor);
                    var historyValue = response.data.needsliderHistories[0].moved_to_value
                    assert.equal(historyValue, startingSliderValueAsPercentage, 'Values in q0_needsliders ' + startingSliderValueAsPercentage + ' and q0_needsliders_histories ' + historyValue + ' are different');
                    needslidersHistoriesCount = response.data.needsliderHistories.length;
                    needslidersHistoriesMostRecentId = response.data.needsliderHistories[0].id;

                    done();
                })
        }),
        it('should create a valid checkin', function(done) {
            var r = checkin.create_checkin(salt, userneedid, relationshipid, wheel_setting, randomComment, checkin_date.getTime() / 1000);
            r.submitCheckin(function(err, response) {
                if (err)
                    console.err(err);
                else {
                    assert(!isNaN(response.data.data.Checkin.id), 'No valid checkin Id was returned');
                    checkinId = response.data.data.Checkin.id;
                }
                done();
            });
        }),
        it('should validate that 1 new need slider entry was created', function(done) {
            needslidersHistory
                .getNeedslidersHistory(needSliderId)
                .done(function(response, reject) {
                    if (reject)
                        done(reject);

                    assert.equal(false, response.error, response.data);
                    assert.equal(true, response.data.needsliderHistories.constructor === Array, response.data.constructor);
                    newCount = response.data.needsliderHistories.length;
                    newId = response.data.needsliderHistories[0].id;
                    assert.equal(needslidersHistoriesCount+1, newCount, 'Expected a new count value of ' + (needslidersHistoriesCount + 1) + ' but got ' + newCount);
                    done();
                })
        }),
        it('should successfully unlink the checkin from the need', function(done) {
            var r = checkin.create_checkin(salt);
            r.unlinkCheckin(checkinId)
                .done(function(response, reject) {
                    if (reject)
                        done(reject);

                    assert.equal(0, response.data.error);
                    assert.equal(null, response.data.data.Checkin.users_need_id, 'Expected users_need_id to be null for checkin ' + checkinId + ' instead received ' + response.data.data.Checkin.users_need_id);
                    done();
                })
        })
    })
})
