var http = require('http');
var assert = require("assert");
var config = require('../config/config');

describe('UserNeeds', function() {
    var email = 'dahannajr@gmail.com';

    describe('#getSunnyStorm()', function() {
        it('should retrieve need summary information for a particular relationship', function(done) {
            // Setup the request.  The options parameter is
            // the o
            var headers = {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Auth-Token': '+l+uQlgOPE/WTLPNVt4/HnHmWezjN/4WwHWTBu1dlOe9QioVUWZlKLIZ9Qn3NSdo'
            };

            var options = {
                host: config.host,
                port: 80,
                path: '/users_needs/sunnyStormy/357.json?filter=sunny',
                method: 'GET',
                headers: headers
            };
            var req = http.request(options, function(res) {
                res.setEncoding('utf-8');

                var responseString = '';

                res.on('data', function(data) {
                    responseString += data;
                });

                res.on('end', function() {
                    var resultObject = JSON.parse(responseString);
                    console.log('Sunny Needs ' + resultObject.data.details.needCount);
                    assert.equal("success", resultObject.data.message);
                    done();
                });
            });

            req.on('error', function(e) {
                done(e);
            });

            req.end();
        }),
        it('should retrieve need summary information for a particular relationship', function(done) {
            // Setup the request.  The options parameter is
            // the o
            var headers = {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Auth-Token': '+l+uQlgOPE/WTLPNVt4/HnHmWezjN/4WwHWTBu1dlOe9QioVUWZlKLIZ9Qn3NSdo'
            };

            var options = {
                host: config.host,
                port: 80,
                path: '/users_needs/sunnyStormy/357.json?filter=stormy',
                method: 'GET',
                headers: headers
            };
            var req = http.request(options, function(res) {
                res.setEncoding('utf-8');

                var responseString = '';

                res.on('data', function(data) {
                    responseString += data;
                });

                res.on('end', function() {
                    var resultObject = JSON.parse(responseString);
                    console.log('Stormy Needs ' + resultObject.data.details.needCount);
                    assert.equal("success", resultObject.data.message);
                    done();
                });
            });

            req.on('error', function(e) {
                done(e);
            });

            req.end();
        })
    })
})
