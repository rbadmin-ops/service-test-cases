var http = require('http');
var assert = require("assert");
var config = require('../config/config');


function makeRandomNeed() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < 10; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return "Random Need " + text;
}

describe('User', function() {
    var email = config.email;
    var authToken = config.auth_token;

    describe('#insertNeeds()', function() {
        it('should insert new needs without error', function(done) {
            // Setup the request.  The options parameter is
            // the object we defined above.

            var need1 = {
                name: makeRandomNeed(),
                trinity_id: 2,
                show_as_public: 0,
                slider_value: 81,
                relationship_id: 1,
                initial_pick: 1
            }; 

            var needSlider1 = {
                slider_value: 81,
                relationship_id: 1
            };


            var need2 = {
                name: makeRandomNeed(),
                trinity_id: 2,
                show_as_public: 0,
                slider_value: 81,
                relationship_id: 1,
                initial_pick: 0
            }; 

            var needSlider2 = {
                slider_value: 81,
                relationship_id: 1
            };

            var needs = [{Need: need1, Needslider: needSlider1}, {Need: need2, Needslider: needSlider2}];
/*            var needs = [
                {Need: {name: "Accepts me as I am",trinity_id: 2, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Ambitious",trinity_id: 2, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Communicates well",trinity_id: 2, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Compassionate",trinity_id: 2, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Easy-going",trinity_id: 2, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Exercises regularly",trinity_id: 2, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Financially secure",trinity_id: 2, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Generous",trinity_id: 2, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Gets along well with my family and friends",trinity_id: 2, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Gets me and my personality",trinity_id: 2, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Good conversationalist",trinity_id: 2, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Great sense of humor",trinity_id: 2, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Has the same religious beliefs",trinity_id: 2, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Honest",trinity_id: 2, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "In great physical shape",trinity_id: 2, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Listens to me",trinity_id: 2, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Loves pets",trinity_id: 2, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Maintains great personal hygiene",trinity_id: 2, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Open minded",trinity_id: 2, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Open to marriage",trinity_id: 2, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Similar sexual needs and frequency",trinity_id: 2, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Strong physical attraction",trinity_id: 2, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Take charge person",trinity_id: 2, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Takes responsibility for their actions",trinity_id: 2, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Tolerates smoking",trinity_id: 2, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Treats everyone with respect",trinity_id: 2, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Very intelligent",trinity_id: 2, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Wants children",trinity_id: 2, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Well paying, steady job",trinity_id: 2, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Appreciates my cooking",trinity_id: 3, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Doesn't watch a lot of TV",trinity_id: 3, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Enjoys a variety of foods",trinity_id: 3, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Enjoys active vacations",trinity_id: 3, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Enjoys amusement parks",trinity_id: 3, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Enjoys arts and crafts",trinity_id: 3, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Enjoys being by the ocean/beach",trinity_id: 3, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Enjoys being in the mountains",trinity_id: 3, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Enjoys being on/by lakes",trinity_id: 3, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Enjoys board games",trinity_id: 3, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Enjoys bowling",trinity_id: 3, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Enjoys camping out",trinity_id: 3, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Enjoys cooking together",trinity_id: 3, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Enjoys dancing",trinity_id: 3, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Enjoys dining out",trinity_id: 3, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Enjoys gambling",trinity_id: 3, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Enjoys gardening",trinity_id: 3, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Enjoys going to concerts",trinity_id: 3, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Enjoys going to the movies",trinity_id: 3, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Enjoys having a pet",trinity_id: 3, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Enjoys hiking",trinity_id: 3, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Enjoys jogging",trinity_id: 3, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Enjoys leisurely vacations",trinity_id: 3, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Enjoys live music",trinity_id: 3, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Enjoys my type of music",trinity_id: 3, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Enjoys reading books",trinity_id: 3, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Enjoys sailing",trinity_id: 3, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Enjoys singing/karaoke",trinity_id: 3, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Enjoys spending time at home",trinity_id: 3, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Enjoys the opera",trinity_id: 3, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Enjoys the theatre/plays",trinity_id: 3, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Enjoys traveling",trinity_id: 3, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Enjoys volunteering",trinity_id: 3, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Enjoys watching TV",trinity_id: 3, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Plays backgammon",trinity_id: 3, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Plays chess",trinity_id: 3, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Plays golf",trinity_id: 3, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Plays tennis",trinity_id: 3, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Practices yoga",trinity_id: 3, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Similar political beliefs",trinity_id: 3, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Always parties",trinity_id: 1, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Bad personal hygiene habits",trinity_id: 1, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Blames others for their problems",trinity_id: 1, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Bullies me",trinity_id: 1, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Constant disagreements and arguing",trinity_id: 1, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Dishonesty",trinity_id: 1, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Disrespects me",trinity_id: 1, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Doesn't pay attention to what I say",trinity_id: 1, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Doesn't want children",trinity_id: 1, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Financially irresponsible",trinity_id: 1, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Flirts with others in front of me or my friends",trinity_id: 1, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Gets drunk often",trinity_id: 1, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Hates pets",trinity_id: 1, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Is close minded",trinity_id: 1, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Is out of shape",trinity_id: 1, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Is sloppy",trinity_id: 1, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Job instability",trinity_id: 1, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Mentally unstable",trinity_id: 1, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Overbearing",trinity_id: 1, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Sexual incompatbility",trinity_id: 1, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Significant anger issues",trinity_id: 1, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Significant ex-spouse issues",trinity_id: 1, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Significant health issues",trinity_id: 1, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Smokes",trinity_id: 1, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Treats people badly",trinity_id: 1, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Unfaithful",trinity_id: 1, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Violent/abusive",trinity_id: 1, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Wants me to change to meet their expectations",trinity_id: 1, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Watches a lot of TV",trinity_id: 1, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 1}, Needslider: needSlider1},
{Need: {name: "Abstains from alcohol",trinity_id: 2, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Accepts my religious beliefs",trinity_id: 2, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Active in politics",trinity_id: 2, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Career oriented",trinity_id: 2, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Cares about environmental issues",trinity_id: 2, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Committed relationship",trinity_id: 2, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Common sense and street smarts",trinity_id: 2, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Deeply spiritual",trinity_id: 2, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Drinks alcohol",trinity_id: 2, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Driven to succeed",trinity_id: 2, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Has good friends",trinity_id: 2, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Has low or no debt",trinity_id: 2, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Has tattoos",trinity_id: 2, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Includes me in their friendships",trinity_id: 2, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Interested in self-improvement",trinity_id: 2, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Is retired",trinity_id: 2, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Loves my tattoos",trinity_id: 2, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Love cats",trinity_id: 2, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Loves dogs",trinity_id: 2, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Monogamous",trinity_id: 2, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Neat and tidy",trinity_id: 2, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "OK with me spending time with my friends",trinity_id: 2, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Ok with stepchildren",trinity_id: 2, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Owns a good home or condo",trinity_id: 2, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Places family first",trinity_id: 2, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Positive relationship with their own family",trinity_id: 2, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Prenuptial agreement",trinity_id: 2, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Pro-choice on abortion",trinity_id: 2, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Pro-life/against abortion",trinity_id: 2, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Religious beliefs come first",trinity_id: 2, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Separation of finances",trinity_id: 2, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Smokes marijuana",trinity_id: 2, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Solid education",trinity_id: 2, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Strives for balance between work and play",trinity_id: 2, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Supports me in public",trinity_id: 2, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Values relationships over things",trinity_id: 2, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Wants to live by the ocean",trinity_id: 2, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Wants to live in the city",trinity_id: 2, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Wants to live in the mountains",trinity_id: 2, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Was married",trinity_id: 2, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Was never married",trinity_id: 2, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Partner to stay home and take care of the kids",trinity_id: 2, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Will live together unmarried",trinity_id: 2, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Willing to relocate",trinity_id: 2, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Willingness for sexual experimentation",trinity_id: 2, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Would consider adopting children",trinity_id: 2, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Cares about animal rights",trinity_id: 3, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Enjoys adventurous vacations",trinity_id: 3, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Enjoys ballroom dancing",trinity_id: 3, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Enjoys casual dining",trinity_id: 3, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Enjoys fine dining",trinity_id: 3, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Enjoys hunting",trinity_id: 3, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Enjoys pet-oriented activities",trinity_id: 3, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Enjoys playing darts",trinity_id: 3, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Enjoys river rafting",trinity_id: 3, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Enjoys roller coasters",trinity_id: 3, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Enjoys salsa dancing",trinity_id: 3, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Enjoys shooting ranges/skeet shooting",trinity_id: 3, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Enjoys short vacations",trinity_id: 3, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Enjoys video games",trinity_id: 3, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Enjoys watching sports with me",trinity_id: 3, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Enjoys/would consider parachuting",trinity_id: 3, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Jogs regularly",trinity_id: 3, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Maintains a healthy diet",trinity_id: 3, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Plays bridge",trinity_id: 3, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Plays poker",trinity_id: 3, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Plays volleyball",trinity_id: 3, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "A lot of debt",trinity_id: 1, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "A religious zealot/evangelist/extremist",trinity_id: 1, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Allergic to my pet",trinity_id: 1, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "An alcoholic",trinity_id: 1, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "An unhealthy diet",trinity_id: 1, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Bad snoring",trinity_id: 1, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Bad table manners",trinity_id: 1, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Disrespectful when we disagree",trinity_id: 1, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Does any drugs",trinity_id: 1, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Does hard drugs",trinity_id: 1, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Doesn't drink alcohol at all",trinity_id: 1, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Doesn't want to work",trinity_id: 1, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Doesn't take good care of themselves",trinity_id: 1, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Expects me to be subservient",trinity_id: 1, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Family that interferes with our life",trinity_id: 1, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Goes to bed angry without talking",trinity_id: 1, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Haphazard lifestyle",trinity_id: 1, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Has tattoos",trinity_id: 1, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Hates cats",trinity_id: 1, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Hates dogs",trinity_id: 1, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Hates their job",trinity_id: 1, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Is boring",trinity_id: 1, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Keeps secrets from me",trinity_id: 1, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Obsessed with politics",trinity_id: 1, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Pro-choice on abortion",trinity_id: 1, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Pro-life/against abortion",trinity_id: 1, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Ridicules me",trinity_id: 1, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Smokes cigars",trinity_id: 1, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Spends recklessly",trinity_id: 1, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Totally different food preferences",trinity_id: 1, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Very materialistic",trinity_id: 1, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Wants to have children",trinity_id: 1, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Wants to have stepchildren",trinity_id: 1, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Wants to relocate",trinity_id: 1, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Was married",trinity_id: 1, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1},
{Need: {name: "Works out obsessively",trinity_id: 1, show_as_public: 1, slider_value: 81, relationship_id: 1, initial_pick: 0}, Needslider: needSlider1}
            ];
*/
            var data = {Needs: needs}

            var needsString = JSON.stringify({data: data});

            var headers = {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Content-Length': needsString.length,
                'Auth-Token': authToken
            };

            var options = {
                host: config.service_host,
                port: config.service_port,
                path: '/needs.json',
                method: 'POST',
                headers: headers
            };
            var req = http.request(options, function(res) {
                res.setEncoding('utf-8');

                var responseString = '';

                res.on('data', function(data) {
                    responseString += data;
                });

                res.on('end', function() {
                    console.log(responseString);
                    var resultObject = JSON.parse(responseString);
                    assert.equal("success", resultObject.data.message);
                    done();
                });
            });

            req.on('error', function(e) {
                done(e);
            });

            req.write(needsString);
            req.end();
        })
    })
})
