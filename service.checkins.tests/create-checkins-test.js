var http = require('http');
var assert = require("assert");
var config = require('../config/config');
var checkin = require('../modules/module.checkins.js');

var randomComment = '';

describe('hooks', function() {
    before(function() {

        randomComment = (makeComment(30));

        console.log(randomEmail);

        function makeComment(len) {
            var text = "";
            var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

            for (var i = 0; i < len; i++)
                text += possible.charAt(Math.floor(Math.random() * possible.length));

            console.log(text);

            return text;
        }
    })
    after(function() {

    })
    beforeEach(function() {

    })
    afterEach(function() {

    })
})

describe('Checkins', function() {
    var cookies;

    describe('#createChecking()', function() {
        it('should create a valid checkin', function(done) {
            var salt = config.auth_token;
            var userneedid = "202";
            var relationshipid = "2";
            var wheel_setting = 1;
            var comment = "randomComment";
            var checkin_date = new Date(2016, 04, 20);

            var r = checkin.create_checkin(salt, userneedid, relationshipid, wheel_setting, comment, checkin_date.getTime() / 1000);
            console.time(comment);

            r.submitCheckin(function(err, response) {
                if (err)
                    console.err(err);
                else {
                    console.timeEnd(comment);

                    console.log(response);
                }

                done();
            });
        })
    })
})
