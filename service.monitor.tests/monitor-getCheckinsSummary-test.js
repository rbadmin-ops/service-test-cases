var http = require('http');
var assert = require("assert");
var config = require('../config/config');

describe('Checkins', function() {
    var email = 'dahannajr@gmail.com';

    describe('#getCheckins()', function() {
        it('should retrieve checkin summary information for the last month', function(done) {
            // Setup the request.  The options parameter is
            // the o
            var headers = {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Auth-Token': '+l+uQlgOPE/WTLPNVt4/HnHmWezjN/4WwHWTBu1dlOe9QioVUWZlKLIZ9Qn3NSdo'
            };

            var options = {
                host: config.host,
                port: 80,
                path: '/monitor/checkinsSummary/357.json?startDate=1441502030',
                method: 'GET',
                headers: headers
            };
            var req = http.request(options, function(res) {
                res.setEncoding('utf-8');

                var responseString = '';

                res.on('data', function(data) {
                    responseString += data;
                });

                res.on('end', function() {
                    var resultObject = JSON.parse(responseString);
                    console.log(resultObject.data.data.details.startDate);
                    console.log(resultObject.data.data.details.endDate);
                    console.log(resultObject.data.data.details.checkinsOverviewMessage);
                    console.log(resultObject.data.data.details.rangedTotalCheckins);
                    assert.equal("success", resultObject.data.message);
                    done();
                });
            });

            req.on('error', function(e) {
                done(e);
            });

            req.end();
        }),
        it('should retrieve checkin summary information for the last month', function(done) {
            // Setup the request.  The options parameter is
            // the o
            var headers = {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Auth-Token': '+l+uQlgOPE/WTLPNVt4/HnHmWezjN/4WwHWTBu1dlOe9QioVUWZlKLIZ9Qn3NSdo'
            };

            var options = {
                host: config.host,
                port: 80,
                path: '/monitor/checkinsSummary/357.json?startDate=1438910030&endDate=1441502030',
                method: 'GET',
                headers: headers
            };
            var req = http.request(options, function(res) {
                res.setEncoding('utf-8');

                var responseString = '';

                res.on('data', function(data) {
                    responseString += data;
                });

                res.on('end', function() {
                    var resultObject = JSON.parse(responseString);
                    console.log(resultObject.data.data.details.startDate);
                    console.log(resultObject.data.data.details.endDate);
                    console.log(resultObject.data.data.details.checkinsOverviewMessage);
                    console.log(resultObject.data.data.details.rangedTotalCheckins);
                    assert.equal("success", resultObject.data.message);
                    done();
                });
            });

            req.on('error', function(e) {
                done(e);
            });

            req.end();
        }),
        it('should retrieve checkin summary information for the last month', function(done) {
            // Setup the request.  The options parameter is
            // the o
            var headers = {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Auth-Token': '+l+uQlgOPE/WTLPNVt4/HnHmWezjN/4WwHWTBu1dlOe9QioVUWZlKLIZ9Qn3NSdo'
            };

            var options = {
                host: config.host,
                port: 80,
                path: '/monitor/checkinsSummary/357.json?startDate=1436318030&endDate=1441502030',
                method: 'GET',
                headers: headers
            };
            var req = http.request(options, function(res) {
                res.setEncoding('utf-8');

                var responseString = '';

                res.on('data', function(data) {
                    responseString += data;
                });

                res.on('end', function() {
                    var resultObject = JSON.parse(responseString);
                    console.log(resultObject.data.data.details.startDate);
                    console.log(resultObject.data.data.details.endDate);
                    console.log(resultObject.data.data.details.checkinsOverviewMessage);
                    console.log(resultObject.data.data.details.rangedTotalCheckins);
                    assert.equal("success", resultObject.data.message);
                    done();
                });
            });

            req.on('error', function(e) {
                done(e);
            });

            req.end();
        }),        
        it('should retrieve checkin summary information for the last month', function(done) {
            // Setup the request.  The options parameter is
            // the o
            var headers = {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Auth-Token': '+l+uQlgOPE/WTLPNVt4/HnHmWezjN/4WwHWTBu1dlOe9QioVUWZlKLIZ9Qn3NSdo'
            };

            var options = {
                host: config.host,
                port: 80,
                path: '/monitor/checkinsSummary/357.json?startDate=1436318030&endDate=1451910030',
                method: 'GET',
                headers: headers
            };
            var req = http.request(options, function(res) {
                res.setEncoding('utf-8');

                var responseString = '';

                res.on('data', function(data) {
                    responseString += data;
                });

                res.on('end', function() {
                    var resultObject = JSON.parse(responseString);
                    console.log(resultObject.data.data.details.startDate);
                    console.log(resultObject.data.data.details.endDate);
                    console.log(resultObject.data.data.details.checkinsOverviewMessage);
                    console.log(resultObject.data.data.details.rangedTotalCheckins);
                    assert.equal("success", resultObject.data.message);
                    done();
                });
            });

            req.on('error', function(e) {
                done(e);
            });

            req.end();
        }),
        it('should retrieve checkin summary information', function(done) {
            // Setup the request.  The options parameter is
            // the o
            var headers = {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Auth-Token': '+l+uQlgOPE/WTLPNVt4/HnHmWezjN/4WwHWTBu1dlOe9QioVUWZlKLIZ9Qn3NSdo'
            };

            var options = {
                host: config.host,
                port: 80,
                path: '/monitor/checkinsSummary/357.json',
                method: 'GET',
                headers: headers
            };
            var req = http.request(options, function(res) {
                res.setEncoding('utf-8');

                var responseString = '';

                res.on('data', function(data) {
                    responseString += data;
                });

                res.on('end', function() {
                    var resultObject = JSON.parse(responseString);
                    console.log(resultObject.data.data.details.startDate);
                    console.log(resultObject.data.data.details.endDate);
                    console.log(resultObject.data.data.details.checkinsOverviewMessage);
                    console.log(resultObject.data.data.details.rangedTotalCheckins);
                    assert.equal("success", resultObject.data.message);
                    done();
                });
            });

            req.on('error', function(e) {
                done(e);
            });

            req.end();
        })
    })
})
