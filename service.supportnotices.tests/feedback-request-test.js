var http = require('http');
var assert = require("assert");
var config = require('../config/config');

describe('SupportNotices', function() {
    var email = 'dahannajr@'

    describe('#feedbackRequest()', function() {
        it('should send a feedback and log it in jira', function(done) {
            // Setup the request.  The options parameter is
            // the object we defined above.

            var supportnotice = {
                body: "TEST TEST I love the tool",
                dropdown: '',
                supportType: 2
            }; 

            var data = {
                Supportnotice: supportnotice
            };

            var info = {
                data: data
            };


            var infoString = JSON.stringify(info);
            //var infoString = 'data%5B_ajaxrequest%5D=1&data%5BUser%5D%5Bemail%5D=dahannajr%40gmail.com&data%5BUser%5D%5Bpassword%5D=Marvin01&data%5BUser%5D%5Bforgotemail%5D=&data%5BUser%5D%5Bdatetime%5D=1432136097';

            var headers = {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Content-Length': infoString.length,
                'Auth-Token': config.auth_token
            };

            var options = {
                host: config.service_host,
                port: config.service_port,
                path: '/supportnotices.json',
                method: 'POST',
                headers: headers
            };
            var req = http.request(options, function(res) {
                res.setEncoding('utf-8');

                var responseString = '';

                res.on('data', function(data) {
                    responseString += data;
                });

                res.on('end', function() {
                    console.log(responseString);
                    var resultObject = JSON.parse(responseString);
                    assert.equal("success", resultObject.data.message);
                    done();
                });
            });

            req.on('error', function(e) {
                done(e);
            });

            req.write(infoString);
            req.end();
        })
    })
})
