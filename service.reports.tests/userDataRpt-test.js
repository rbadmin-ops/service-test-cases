var http = require('http');
var assert = require("assert");
var config = require('../config/config');

describe('User', function() {

    describe('#reportAccess()', function() {
        it('should download report without error', function(done) {
            // Setup the request.  The options parameter is
            // the object we defined above.

            var headers = {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Content-Length': 0,
                'Auth-Token': 'R4xnvcdDjhfsqe381z5FuH+Vi3z5OGDh7m91b4tnX9Lzx5C6Ch7S98e/85/9OVh9'
            };

            var options = {
                host: config.service_host,
                port: config.service_port,
                path: '/reporting.php',
                method: 'GET',
                headers: headers
            };
            var req = http.request(options, function(res) {
                res.setEncoding('utf-8');

                var responseString = '';

                res.on('data', function(data) {
                    responseString += data;
                });

                res.on('end', function() {
                    console.log(responseString);
                    done();
                });
            });

            req.on('error', function(e) {
                done(e);
            });

            req.write('');
            req.end();
        })
    })
})
