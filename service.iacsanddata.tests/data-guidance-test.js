var http = require('http');
var assert = require("assert");
var config = require('../config/config');


describe('Guidance', function() {
    var cookies;

    describe('#getGuidanceInformation()', function() {
        it('should retrieve all guidance information', function(done) {
            var headers = {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Content-Length': 0,
                'Auth-Token': '+l+uQlgOPE/WTLPNVt4/HnHmWezjN/4WwHWTBu1dlOe9QioVUWZlKLIZ9Qn3NSdo'
            };

            var options = {
                host: config.service_host,
                port: config.service_port,
                path: '/guidance.json',
                method: 'GET',
                headers: headers
            };
            var req = http.request(options, function(res) {
                res.setEncoding('utf-8');

                var responseString = '';

                res.on('data', function(data) {
                    responseString += data;
                });

                res.on('end', function() {
                    console.log(responseString);
                    var resultObject = JSON.parse(responseString);
                    done();
                });
            });

            req.on('error', function(e) {

                done(e);
            });

            req.write('');
            req.end();
        })
    })
})
