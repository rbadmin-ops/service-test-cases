var http = require('http');
var assert = require("assert");
var config = require('../config/config');
var registration = require('../modules/module.registration.js');
var user = require('../modules/module.user');

var randomEmail = '';
var registrant = registration.create_registration(makeRandomString(10) + '@kickstepinc.com', '1111', '1111');


function makeRandomString(len) {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < len; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}

describe('User', function() {
    var cookies;

    before(function() {
        randomEmail = (makeRandomString(15)) + '@gmail.com';

    });

    describe('#registration()', function() {
        it('should identify email address as already taken', function(done) {
            var headers = {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Content-Length': 0
            };

            var options = {
                host: config.service_host,
                port: config.service_port,
                path: '/UserProfiles/validateEmail.json?email=dahannajr@gmail.com',
                method: 'GET',
                headers: headers
            };
            var req = http.request(options, function(res) {
                res.setEncoding('utf-8');

                var responseString = '';

                res.on('data', function(data) {
                    responseString += data;
                });

                res.on('end', function() {
                    cookies = res.headers['set-cookie']
                    var resultObject = JSON.parse(responseString);
                    assert.equal("1", resultObject.data.error);
                    assert.equal("The email address of [dahannajr@gmail.com] has already been used.", resultObject.data.message);
                    done();
                });
            });

            req.on('error', function(e) {

                done(e);
            });

            req.write('');
            req.end();
        }),
        it('should identify email address as available', function(done) {
            var headers = {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Content-Length': 0
            };

            var options = {
                host: config.service_host,
                port: config.service_port,
                path: '/UserProfiles/validateEmail.json?email=' + randomEmail,
                method: 'GET',
                headers: headers
            };
            var req = http.request(options, function(res) {
                res.setEncoding('utf-8');

                var responseString = '';

                res.on('data', function(data) {
                    responseString += data;
                });

                res.on('end', function() {
                    cookies = res.headers['set-cookie'];
                    var resultObject = JSON.parse(responseString);
                    assert.equal("0", resultObject.data.error);
                    assert.equal("The email address of [" + randomEmail + "] does not exist.", resultObject.data.message);
                    done();
                });
            });

            req.on('error', function(e) {

                done(e);
            });

            req.write('');
            req.end();
        }),
        it('should allow registration with same passwords', function(done) {
            registrant.registerUser(function(err, response) {
                if (err)
                    done(err);
                else {
                    assert.equal("0", response.data.error);
                    assert.equal("success", response.data.message);
                    assert.equal(true, registrant.userObj.salt.length > 0);

                    registrant.userObj.username = makeRandomString(15);

                    done();
                }
            });
        }),
        it('should allow for username initialization', function(done) {
            registrant.initializeUsername(function(err, response) {
                if (err)
                    done(err);
                else {
                    assert.equal("0", response.data.error);
                    assert.equal(registrant.userObj.username, response.data.data.User.username);

                    registrant.userObj.add_relationship('Heather', '1999-10-25', '0000-00-00', 'No', 1, 70);
                    done();
                }
            });

        }),
        it('should allow for a relationship to be added', function(done) {
            registrant.addRelationship(0, function(err, response) {
                if (err)
                    done(err);
                else {
                    assert.equal("0", response.data.error);
                    assert.equal("success", response.data.message);
                    assert.equal(response.data.data.Relationship.name, registrant.userObj.relationships[0].Relationship.name);

                    done();
                }
            });
        }),
        it('should allow for the full profile to be added', function(done) {
            registrant.addProfile(function (err, response) {
                if (err)
                    done(err)
                else {
                    assert.equal("0", response.data.error);
                    assert.equal("The user profile has been saved.", response.data.message);

                    done();
                }
            });
        })
    })
})
