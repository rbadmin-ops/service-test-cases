var http = require('http');
var assert = require("assert");
var config = require('../config/config');

describe('UserProfiles', function() {
    var email = 'dahannajr@'

    describe('#updatePassword()', function() {
        it('should update password without error', function(done) {
            // Setup the request.  The options parameter is
            // the object we defined above.

            var userProfile = {                
                password: 'Marvin01'
            }; 

            var user = {
//                email: 'dahannajr@gmail.com'
                email: 'david.hanna@kickstepinc.com'
            };

            var status = {
                relationship_status: 4
            };

            var fullProfileUpdate = {
                User: user,
                Status: status
            };

            var fullProfileUpdateString = JSON.stringify(fullProfileUpdate);
            var userString = 'data%5BUser%5D%5Bpassword%5D=Marvin02';

            //fullProfileUpdateString = '{"UserProfile":{"id":"4","birth_day":"1975-04-06T04:00:00.000Z","gender":"1","interested_in":"2","divorced":"1","password":"Marvin01","password_new":"Marvin02","password_confirm":"Marvin02"},"User":{"email":"dahannajr@gmail.com"},"Status":{"relationship_status":"4"}}';

            var headers = {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Content-Length': userString.length,
                'Auth-Token': '+l+uQlgOPE/WTLPNVt4/HnHmWezjN/4WwHWTBu1dlOe9QioVUWZlKLIZ9Qn3NSdo'
            };

            var options = {
                host: config.host,
                port: 80,
                path: '/UserProfiles/changePassword.json',
                method: 'POST',
                headers: headers
            };

            var req = http.request(options, function(res) {
                res.setEncoding('utf-8');

                var responseString = '';

                res.on('data', function(data) {
                    responseString += data;
                });

                res.on('end', function() {
                    console.log(responseString);
                    var resultObject = JSON.parse(responseString);
                    assert.equal("The user profile has been saved.", resultObject.data.message);
                    done();
                });
            });

            req.on('error', function(e) {
                done(e);
            });

            req.write(userString);
            req.end();
        })
    })
})
