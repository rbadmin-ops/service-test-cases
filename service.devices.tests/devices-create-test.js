var http = require('http');
var assert = require("assert");
var config = require('../config/config');
var device = require('../../rb-service-tools')().Device;
var _ = require('lodash');
//var device = require('rb-services-tools').Device;

describe('Devices', function() {
    var deviceId;

    describe('#addNewDevice', function() {
        it('should create a new device', function(done) {
            var deviceObj = device
                .createDeviceModel(2, device.createUniqueGUID(), 'device model', 'device os', 'app version')

            device.createV2DeviceServiceInterface(config)
                .addDevice(deviceObj)
                .done(function(response, reject) {
                    if (reject) {
                        done(reject)
                    }

                    assert.strictEqual(response.constructor, Object, 'No data object returned');
                    assert.strictEqual(response.message, 'success');
                    assert.equal(false, isNaN(response.device.id), "Expected id to be an integer")

                    deviceId = response.device.id;
                    done();
                })
        }),
        it('should update the device last_used', function(done) {
            sleep(2000).then(() =>
            device.createV2DeviceServiceInterface(config)
                .updateDeviceLastUsed(deviceId)
                .done(function(response, reject) {
                    if (reject) {
                        done(reject)
                    }

                    done();
                })
            );
        }),
        it('should find several devices last used by user id', function(done) {
            device.createV2DeviceServiceInterface(config)
                .findByLastUsedUserId(2)
                .done(function(response, reject) {
                    if (reject) {
                        done(reject);
                    }


                    assert.equal(Array, response.data.constructor , "Expected response.data to be an array")
                    assert.equal(false, isNaN(response.data[0].id), "Expected id to be an integer")

                    done();
                })
        })
    })
})

function sleep (time) {
  return new Promise((resolve) => setTimeout(resolve, time));
}
